## Description

Classic [snake game](https://en.wikipedia.org/wiki/Snake_(video_game_genre)) written in elixir and javascript

## Architecture

It is a basic phoenix elixir project. The most game logic is concentrated into `priv/static/js/myjs.js`.
There we are looking for a `canvas` html tag where we create needed elements.

Each time the game is over we are sending the current score to end point where we
store the result in NoSQL db (for simplicity DETS, just key/val db)

## Gameplay

The purpose is to `eat` `apples`(the array in our code is called `food`) as much as possible. Every time we've made 10 points we
are moving to the next level, where the snake moves faster.

At the beginig the user have to input his username

## User interface

We can start new game, see the previous scores and in the about me page


## About the project

it has very educational purpose. The code is very basic and has no clames to be
complete and polished.

## Todo

- more documentation
- refactoring
- exunit tests
- fix some bugs - like the food is spawned on top of the snake, and the snake moves through the scores

## New possible features

Using the blockchain to store the scores instead of local db. For that purpose I may use the aeternity
[elixir-research project](https://github.com/aeternity/elixir-research)