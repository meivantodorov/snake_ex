defmodule SnakeWeb.PageController do
  use SnakeWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def display_scores(conn, _params) do
    render conn, "display_scores.html"
  end

  def about(conn, _params) do
    render conn, "about.html"
  end

  def new_score(conn, params) do
    name  = String.replace(conn.query_params["name"], "\"", "")
    score = String.replace(conn.query_params["score"], "\"", "")
    :ok   = SnakeWeb.Scores.add_new_score_to_db(name, score)
    json(conn, "success")
  end

end
