defmodule SnakeWeb.Scores do
  @moduledoc """
  This module handles adding and getting score from the db
  Before new score it is about to be added we check first
  if is higher than the previous
  """

  @db :scores_db

  @spec get_scores() :: list()
  def get_scores() do
    {:ok, table} = :dets.open_file(@db, [type: :set])
    list =
      case :dets.lookup(table, :scores) do
        [scores: list] -> list
        [] -> []
      end
    :dets.close(table)
    Enum.sort(list)
  end

  @spec add_new_score_to_db(name :: String.t(), score :: String.t()) :: atom()
  def add_new_score_to_db(name, score) do
    ## TODO : refactoring on this function, has too many nested cases
    {:ok, table} = :dets.open_file(@db, [type: :set])
    case :dets.lookup(table, :scores) do
      [scores: list] ->
        case List.keyfind(list, name, 0) do
          {k, v} ->
            if String.to_integer(score) > String.to_integer(v) do
              list = list -- [{k,v}]
              :dets.insert(table, {:scores, [{name, score} | list]})
            end
          nil ->
            :dets.insert(table, {:scores, [{name, score} | list]})
        end
      [] ->
        :dets.insert(table, {:scores, [{name, score}]})
    end
    :dets.close(table)
    :ok
  end

end
